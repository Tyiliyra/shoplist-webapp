var productsJson = [];
var productsCount = 0;
const createListUrl = "https://shoplist-webapp.herokuapp.com/shopping_list"
var isListening = false;
var autoRestart;
var lastStartedAt = 0;
var timeSinceLastStart = 0;
var isHidden = true;

// show existing shopping lists
function getList() {
    $.get(createListUrl + "/getShoppingListNames", function (data, status) {
        if (data != '3') {
            // pupulating the select id="listFind" from the db with the existing lists
            var listSelect = document.getElementById("listSelect")
            while (listSelect.lastChild) {
                listSelect.removeChild(listSelect.lastChild);
            }
            for (let i = 0, len = data.length; i < len; i++) {
                var opt = document.createElement("option")
                opt.innerHTML = data[i].name;
                listSelect.appendChild(opt);
            }
        }
    });
}

$(document).ready(function () {
    getList();
});

// hide products from the previous list
document.getElementById("listSelect").addEventListener("change", (e) => {
    $.post(createListUrl + "/get_shopping_list", { shoppingList_name: e.target.value }, function (data, status) {
        var close = document.getElementsByClassName("close");
        for (let i = 0, len = close.length; i < len; i++) {
            close[i].click();
        }
        productsCount = 0;
        document.getElementById("writeName").value = e.target.value;
        //show current list products
        for (let i = 0, len = data.length; i < len; i++) {
            addNewItem(data[i].name);
        }
    });
});

// speech recognition api
window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition;
// window.SpeechGrammarList = window.webkitSpeechGrammarList || window.SpeechGrammarList;
if (window.SpeechRecognition) {
    var recognition = new SpeechRecognition();
    // grammar has to be set if the purpose is to get exact results
    // as it is left aside app records any spoken words
    // var speechRecognitionList = new SpeechGrammarList();
    // speechRecognitionList.addFromString(grammarPL);
    // recognition.grammars = speechRecognitionList;
    // the default lang is the html doc used language so the next line is unnecesary
    // recognition.lang = "en";
    // there is no need for alternatives since the app does not have any grammar comparison source
    recognition.interimResults = false;
    // recognition.maxAlternatives = 3;
    // recognition.continous turned off for faster results in https
    recognition.continuous = false;
    recognition.onresult = (e) => {
        var finalTranscript = '';
        for (let i = e.resultIndex, len = e.results.length; i < len; i++) {
            let transcript = e.results[i][0].transcript;
            if (e.results[i].isFinal) {
                finalTranscript = transcript;
                break;
            }
        }

        document.getElementById("myInput").value = finalTranscript;
        document.getElementById("addItem").click();
    }
    recognition.start();

    recognition.onstart = () => {
        lastStartedAt = new Date().getTime();
        isListening = true;
        autoRestart = true;
    }

    recognition.onend = function () {
        isListening = false;
        // application will auto restart if it is closed automatically and not by user action
        if (autoRestart) {
            // disable the application from starting to often
            timeSinceLastStart = new Date().getTime() - lastStartedAt;
            if (timeSinceLastStart > 1000) {
                recognition.start();
            }
        }
    }
}

// to do list
// create a "close" button and append it to each list item
var myNodelist = document.getElementsByTagName("LI");
for (let i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}

// add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);

// create a new list item
function addNewItem(itemName) {
    var li = document.createElement("li");
    var t = document.createTextNode(itemName);
    li.appendChild(t);
    if (itemName === '') {
        alert("Input field (products) cannot be empty")
    }
    else {
        document.getElementById("myUL").appendChild(li);
        productsJson[productsCount] = '{ "name": "' + itemName + '" }';

        document.getElementById("myInput").value = "";

        var span = document.createElement("SPAN");
        var txt = document.createTextNode("\u00D7");
        span.className = "close";
        span.appendChild(txt);
        li.appendChild(span);

        var close = document.getElementsByClassName("close");
        var ind = close.length - 1;
        var prdCountClosure = productsCount;
        close[ind].onclick = function () {
            var div = this.parentElement;
            div.style.display = "none";
            productsJson[prdCountClosure] = "";
        }

        productsCount++;
    }
}

// create a new list item when clicking on the "Add" button
document.getElementById("addItem").addEventListener("click", function() {
    var inputValue = document.getElementById("myInput").value;
    addNewItem(inputValue);
});

// saving list with its products to the database
document.getElementById("listSave").addEventListener("click", function () {
    var finalJson = '{ "shoppingList_name": "';
    var listName = document.getElementById("writeName").value;
    if (listName === '') {
        alert("Input field (list name) cannot be empty");
        return;
    }
    finalJson += listName + '", "products": [';
    var isFirst = true;
    for (let i = 0, len = productsJson.length; i < len; i++) {
        var prod = productsJson[i];
        if (prod !== '') {
            if (isFirst) {
                finalJson += prod;
                isFirst = false;
            }
            else {
                finalJson += ", " + prod;
            }
        }
    }
    finalJson += "]}";
    $.post(createListUrl + "/save_shopping_list", JSON.parse(finalJson), function (data, status) {
        if (data == '0') {
            alert('Your list has been created successfuly');
            getList();
        }
        else if (data == '1')
            alert('Shopping list name is already taken');
        else if (data == '2')
            alert('Failed to create new products');
        else
            alert('Unexpected error occured');
        // console.log('data is ' + data);
    });
});

// show / hide tips
document.getElementById("questionMark").addEventListener("click", () => {
    if (isHidden) {
        document.getElementById("tips").classList.remove("noDisplay");
        isHidden = false;
    }
    else {
        document.getElementById("tips").classList.add("noDisplay");
        isHidden = true;
    }
});

// printing content
document.getElementById("listPrint").addEventListener("click", () => {
    window.print();
});