function setLanguage() {
    const selectedLang = document.getElementById("selectLang").value;
    if (selectedLang == "polski") {
        // setting cookie
        document.cookie = "language=polski; max-age=3600; path=/";
        var langPLArray = document.getElementsByClassName("langPL")
        for (let i = 0, len = langPLArray.length; i < len; i ++) {
            langPLArray[i].classList.remove("noDisplay");
        }
        var langENArray = document.getElementsByClassName("langEN")
        for (let i = 0, len = langENArray.length; i < len; i ++) {
        langENArray[i].classList.add("noDisplay");
        }
    }
    else {
        // setting cookie
        document.cookie = "language=English; max-age=3600; path=/";
        var langENArray = document.getElementsByClassName("langEN")
        for (let i = 0, len = langENArray.length; i < len; i ++) {
            langENArray[i].classList.remove("noDisplay");
        }
        var langPLArray = document.getElementsByClassName("langPL")
        for (let i = 0, len = langPLArray.length; i < len; i ++) {
            langPLArray[i].classList.add("noDisplay");
        } 
    }
}

window.onload = function () {
    document.getElementById("selectLang").addEventListener("change", setLanguage);
    if (document.cookie.length != 0) {
        var nameValueArray = document.cookie.split("=");
        document.getElementById("selectLang").value = nameValueArray[1];
        setLanguage();
    } 
}