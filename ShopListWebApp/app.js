﻿'use strict';
var debug = require('debug');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// zaladowanie kontrolerow do zmiennych
var indexControler = require('./routes/indexControler');
var shoppingListControler = require('./routes/shoppingListControler');

//glowny obiekt serwera
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views')); //the first is inbuild parameter and second is path
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // very important, because the default boolean value is false, must be changed or the conversion syntax of json html will be problematic
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); // dodanie folderu, od którego można relatywnie wpisywać ścieżki plików klienta!!

// definicje routingu (przypisanie bazowych adresów do kontrolerów obslugujacych requesty przychodzace na dany adres)
app.use('/', indexControler);
app.use('/shopping_list', shoppingListControler);

// error handlers
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    //wpisac err.status || 500 w gugel
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});
