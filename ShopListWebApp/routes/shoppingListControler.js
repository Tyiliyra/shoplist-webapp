﻿'use strict';
var express = require('express');
var router = express.Router();
var sql = require('../db.js');

router.get('/', function (req, res) {
    res.render('shoppingList.ejs');
});

router.get('/getShoppingListNames', function (req, res) {
    sql.query("SELECT name FROM shopping_list", function (error, results, fields) {
        if (error || results.length == 0) {
            console.log('error ' + error);
            res.status(200).send('3'); //sql query failed
        }
        else {
            res.json(results);
        }
    });
});

// get all the products from list with given name
router.post('/get_shopping_list', function (req, res) {
    sql.query("SELECT id FROM shopping_list WHERE name = '" + req.body.shoppingList_name + "'", function (error, results, fields) {
        if (error || results.length == 0) {
            console.log('error ' + error);
            res.status(200).send('3'); //sql query failed
        }
        else {
            console.log();
            sql.query('SELECT name FROM product WHERE shopping_list_id = ' + results[0].id, function (error, results, fields) {
                if (error) {
                    console.log('error ' + error);
                    res.status(200).send('3'); //sql query failed
                }
                else {
                    res.json(results);
                }
            });
        }
    });
    
});

/* POST users listing. */
router.post('/save_shopping_list', function (req, res) {
    /* req.body contains json sent from client by post method, json cannot be sent with get method */
    console.log(req.body.shoppingList_name);
    console.log(req.body.products.length);
    sql.query('INSERT INTO shopping_list (name) VALUES ("' + req.body.shoppingList_name + '");', function (error, results, fields) {
        if (error) {
            console.log('error ' + error);
            res.status(200).send('1'); //the name was already taken
        }
        else {
            var shoppingListId = results.insertId;
            var productRecords = [];
            for (let i = 0, len = req.body.products.length; i < len; i++) {
                productRecords[i] = [shoppingListId, req.body.products[i].name];
            }
            sql.query('insert INTO product (shopping_list_id, name) VALUES ?', [productRecords], function (err, results, fields) {
                if (err) {
                    console.log('error ' + err);
                    res.status(200).send('2'); //SQL query failed due to non existing product table / columns or incorrect query
                }
                else {
                    res.status(200).send('0'); //Everything done correctly
                }
            });
        }
        //else if (results.length == 0)
        //    res.render('error', { error: -1, message: "Nie ma takich rekordów w bazie no kurła..." });
        //else {
        //    //results is array of Jsons, each json being a record returned by the sql query
        //    res.render('categories', { categoriesTag: results });
        //}
    });
});

module.exports = router;
